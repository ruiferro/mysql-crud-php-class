<?php

/*
###############################
# KSoftWare Productions       #
# Rui Simões - 25/06/2016     #
# Daniel Maduro - 25/06/2016  #
# http://ruiferrolho.com/     #
###############################
*/

class MySQL {

    private $host = "localhost"; //Mudar para o seu endereço
    private $user = "root"; //Mudar para o seu utilizador
    private $password = "qwerty"; //Mudar para a sua password
    private $db = "crucialsports"; //Mudara para o nome da base de dados

	private function Con() {
		try {

			$Con = new PDO('mysql:dbname='.$this->db.';host='.$this->host.'', $this->user , $this->password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

			return $Con;

		} catch (Exception $e) {	

			return ("MySQL Connection error");

			die();

		}

	}

	public function read($table,$where=null) {

        $con = $this->Con();

        $query = "";

        if ($where==null) {
            //Select All
            $query = "SELECT * FROM ".$table;

        } else {
            //Select Where 
            $where = array_slice($where,0,1);
            $query = "SELECT * FROM ".$table." WHERE ".key($where)." = '".current($where)."';";
        }

        try {
            $sql = $con->prepare($query);
            $sql->execute();
            $content = $sql->fetchAll(PDO::FETCH_ASSOC);

            return $content;
        } catch (PDOExecption $e) {
            return "Error: ".$e;
        }


	}

	public function update($table,$content,$where=null) {
        $con = $this->Con();

        $str = "";

        if ($where==null) {
                $str = "UPDATE ".$table." SET ";
                $count = count($content);
                $counter = 0;

                foreach ($content as $assoc => $cont) {

                    if ($counter == ($count-1)) {
                        $str = $str.$assoc."='".$cont."';";
                    } else {
                        if ($counter == 0) {
                            $str = $str.$assoc."='".$cont."',";
                        } else {
                            $str = $str.$assoc."='".$cont."',";
                        }
                    }

                    $counter++;
                }

        } else {

                //Update Where
                $str = "UPDATE ".$table." SET ";
                $count = count($content);
                $counter = 0;

                foreach ($content as $assoc => $cont) {
                    $counter++;
                    if ($count == 1) {
                        $str.= $assoc."='".$cont."'";
                    } elseif ($counter == $count) {
                        $str.= $assoc."='".$cont."'";
                    } else {
                        $str.= $assoc."='".$cont."',";
                    }
                }

                //Fazer o Where
                foreach ($where as $assoc => $cont) {
                    $str.=" WHERE ".$assoc."='".$cont."';";
                }
        }

        $sql = $con->prepare($str);

        try {
            $sql->execute();
            return true;
        } catch (PDOExecption $e) {
            return "Error: ".$e;
        }	

	}

	public function insert($table,$content) {

		$con = $this->Con();

		$str = "INSERT INTO ".$table." (";

		$count = count($content);

		$counter = 0;

		foreach ($content as $assoc => $cont) {
			$counter++;
			if ($count == 1) {
				$str.= $assoc.")";
			} elseif ($counter == $count ) {
				$str.= $assoc.")";
			} else {
				$str.= $assoc.",";
			}
		}

		//Values
		$str.= " VALUES ";
		$counter = 0;

		foreach ($content as $assoc => $cont) {
			$counter++;
			if ($counter == 1) {
				$str.= "('".$cont."',";
			} elseif ($counter == $count){
				$str.= "'".$cont."')";
			} else {
				$str.= "'".$cont."',";
			}
		}

		$str.= ";";

		//Executar a Query
		$sql = $con->prepare($str);

		try {
			$sql->execute();
			return true;
		} catch (PDOExecption $e) {
			return "Error: ".$e;
		}

	}

	public function delete($table,$where=null) {
		$con = $this->Con();

        if ($where==null) {
		    $query = "DELETE FROM ".$table.";";
        } else {
            //Delete Where
            $where = array_slice($where,0,1);
            $query = "DELETE FROM ".$table." WHERE ".key($where)."='".current($where)."';";
        }

		$sql = $con->prepare($query);

		try {
			$sql->execute();
			return true;
		} catch (PDOExecption $e) {
			return "Error: ".$e;
		} 
	}

    public function executeReader($query) {
        try {
            $con = $this->Con();
            $sql = $con->prepare($query);
            $sql->execute();
            return $sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOExecption $e) {
            return "Error: ".$e;
        }
    }

    public function executeNonQuery($query) {
        try {
            $con = $this->Con();
            $sql = $con->prepare($query);
            $sql->execute();
            return true;
        } catch (PDOExecption $e) {
            return "Error: ".$e;
        }
        

    }
}
